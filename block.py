import json
import os
import hashlib

blockchain_dir = os.curdir + '/blockchain/'

def get_hash(file_name):
    with open(blockchain_dir + str(file_name), 'rb') as f:
        return hashlib.md5(f.read()).hexdigest()


def get_files():
    files = os.listdir(blockchain_dir)
    return sorted([int(x) for x in files])


def check_integrity():
    # 1. Считать хеш предыдущего блока
    # 2. Вычислить хеш предыдущего блока
    # 3. Сравнить полученные данные
    files = get_files()
    results = []

    for file in files[1:]:
        with open(blockchain_dir + str(file)) as f:
            # получим хеш (хеш предыдущего блока), записанный в текущий файл, в поле hash
            file_hash = json.load(f)['hash']
            # заново рассчитаем хеш предыдущего блока
            prev_file = file - 1
            actual_hash = get_hash(prev_file)

            prev_file = str(prev_file)

            if file_hash == actual_hash:
                res = 'OK'
            else:
                res = 'Corrupted'

            #print('Block {} is: {}'.format(prev_file, res))
            results.append({"block": prev_file, "result": res})

    return results

def write_block(person, amount, to_whom, prev_hash=''):
    files = get_files()

    prev_file = files[-1]

    new_file_name = str(prev_file + 1)

    prev_hash = get_hash(prev_file)

    data = {
          "person": person,
          "amount": amount,
          "to_whom": to_whom,
          "hash": prev_hash
        }

    with open(blockchain_dir + new_file_name, 'w') as file:
        json.dump(data, file, indent = 4, ensure_ascii = False)


def main():
    #write_block(person='Alex Petrov', amount='70000', to_whom='Alexey Denisov')
    print(check_integrity())


if __name__ == '__main__':
    main()
